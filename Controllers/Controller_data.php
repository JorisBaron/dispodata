<?php
	class Controller_data extends Controller
	{

		public function action_recherche()
		{
			if(test_login()) {
				$m=Model::get_model();
				$villes = $m->getVillesSelectRecherche();
				$this->render("recherche",array(
					"villes"=>$villes
				));
			}
			else {
				redirect('?controller=connexion&action=connexion');
			}

		}

		function action_traitement_recherche() {
			session_start_once();
			if(isset($_POST['insee']) && preg_match("#^[0-9]{5}$#",trim($_POST['insee']))) {
				$_SESSION['insee']=trim($_POST['insee']);
				redirect("?controller=data&action=recap");
			}
			else {
				$this->action_recherche();
			}
		}

		function action_recap() {
			if(test_login())
			{
				if(isset($_SESSION['insee']))
				{
					$m = Model::get_model();
					$insee = $_SESSION['insee'];
					$infosVille = $m->getInfosVille($insee);
					if($infosVille)
					{

						////////////////////
						// METEO / CLIMAT //
						////////////////////
						$tabCoordMeteo = $m->getStationsMeteo();
						$min = calculOrthodromie($infosVille['longitude'], $infosVille['latitude'], $tabCoordMeteo[0]['longitude'], $tabCoordMeteo[0]['latitude']);
						$indicatif = $tabCoordMeteo[0]['indicatif'];
						for($i = 1; $i < count($tabCoordMeteo); $i++)
						{
							$calc = calculOrthodromie($infosVille['longitude'], $infosVille['latitude'], $tabCoordMeteo[$i]['longitude'], $tabCoordMeteo[$i]['latitude']);
							if($calc < $min)
							{
								$min = $calc;
								$indicatif = $tabCoordMeteo[$i]['indicatif'];
							}
						}
						$donneesMeteo = $m->getDonneesMeteoRecap($indicatif);

						$donneesFinalesMeteo=array();
						$key="";
						foreach($donneesMeteo as $cle=>$tab) {
							if($cle!='commune')
							{
								switch($cle)
								{
									case 'nbJoursTempMaxSup25':
										$key="Nombre de jours avec T° max &ge; 25°C";
										break;
									case 'hauteurPrecipitationsMoyenne':
										$key="Hauteur de précipitations (moyenne en mm)";
										break;
									case 'degresJoursUnifies':
										$key='Degrés Jours Unifié (moyenne en °C)';
										break;
									case 'dureeInsolation':
										$key='Durée d\'insolaton (moyenne en heures)';
										break;
								}

								if($tab!=null)
								{
									$donneesFinalesMeteo[$key]=explode(";", $tab);
								}
								else
								{
									$donneesFinalesMeteo[$key]=null;
								}
							}
						}

						//////////////////
						// BIODIVERSITE //
						//////////////////

						$donneesINPN=$m->getDonneesINPNRecap($insee);
						foreach($donneesINPN as &$ligne) {
							if($ligne['nb']==null) {
								$ligne['nb']=0;
							}
						}
						///////////
						// RADON //
						///////////

						$donneeRadon=$m->getDonneesRadon($insee);
						
						////////////
						// GASPAR //
						////////////
						
						$donneeGASPAR = $m->getDonneesGASPARRecap($insee);
						$donneeGASPARfinales = array();
						foreach($donneeGASPAR as $donnee) {
							$donneeGASPARfinales[$donnee['name']]=$donnee['count'];
						}
						
						////////////
						// RENDER //
						////////////
						$this->render('recap', array(
							"donneesMeteo" => $donneesFinalesMeteo,
							"stationMeteo"=>$donneesMeteo['commune'],
							"commune" => $infosVille['nom'],
							"donneesINPN" => $donneesINPN,
							"donneeRadon" => $donneeRadon['classe_potentiel'],
							"donneesGASPAR"=> $donneeGASPARfinales
						));
						
						
						
					}
				}
				else {
					$this->action_recherche();
				}
			}
			else {
				redirect('?controller=connexion&action=connexion');
			}



			//TODO

			//$this->render("message",array("title"=>"Indisponible","message"=>"Le développement de cette page est encore en cours"));
		}

		public function action_meteo() {
	      if(test_login())
	      {
	        if(isset($_SESSION['insee']))
	        {
	          $m = Model::get_model();
	          $insee = $_SESSION['insee'];
	          $infosVille = $m->getInfosVille($insee);
	          if($infosVille)
	          {
	            $tabCoordMeteo = $m->getStationsMeteo();
	            $min = calculOrthodromie($infosVille['longitude'], $infosVille['latitude'], $tabCoordMeteo[0]['longitude'], $tabCoordMeteo[0]['latitude']);
	            $indicatif = $tabCoordMeteo[0]['indicatif'];
	            for($i = 1; $i < count($tabCoordMeteo); $i++)
	            {
	              $calc = calculOrthodromie($infosVille['longitude'], $infosVille['latitude'], $tabCoordMeteo[$i]['longitude'], $tabCoordMeteo[$i]['latitude']);
	              if($calc < $min)
	              {
	                $min = $calc;
	                $indicatif = $tabCoordMeteo[$i]['indicatif'];
	              }
	            }
	            $donneesMeteo = $m->getDonneesMeteo($indicatif);

	            $donneesFinalesMeteo=array();
	            $key="";
	            foreach($donneesMeteo as $cle=>$tab) {
	              if($cle!='commune')
	              {
	                switch($cle)
	                {
	                  case 'temperatureMax':
	                    $key="Température maximale moyenne (en °C)";
	                    break;
	                  case 'temperatureMin':
	                    $key="Température minimale moyenne (en °C)";
	                    break;
	                  case 'nbJoursTempMaxSup25':
	                    $key="Nombre de jours avec T° max &ge; 25°C";
	                    break;
	                  case 'nbJoursTempMaxInf0':
	                    $key="Nombre de jours avec T° max &le; 0°C";
	                    break;
	                  case 'recordHauteurPrecipitations':
	                    $key="Record de hauteur quotidienne maximale de précipitations (en mm)";
	                    break;
	                  case 'hauteurPrecipitationsMoyenne':
	                    $key="Hauteur de précipitations (moyenne en mm)";
	                    break;
	                  case 'degresJoursUnifies':
	                    $key='Degrés Jours Unifié (moyenne en °C)';
	                    break;
	                  case 'rayonnementGlobal':
	                    $key="Rayonnement global (moyenne en J/cm²)";
	                    break;
	                  case 'dureeInsolation':
	                    $key='Durée d\'insolaton (moyenne en heures)';
	                    break;
	                  case 'recordRafaleVent':
	                    $key="Record de la rafale de vent (en m/s)";
	                    break;
	                  case 'nbJoursNeige':
	                    $key="Nombre moyen de jours avec de la neige";
	                    break;
	                }

	                if($tab!=null)
	                {
	                  $donneesFinalesMeteo[$key]=explode(";", $tab);
	                }
	                else
	                {
	                  $donneesFinalesMeteo[$key]=null;
	                }
	              }
	            }
	            $this->render('meteo', array(
								"donneesMeteo" => $donneesFinalesMeteo,
								"stationMeteo"=>$donneesMeteo['commune'],
								"commune" => $infosVille['nom']
							));

	          }
	        }
				}
			}

		public function action_sol() {
			if(test_login())
			{
				if(isset($_SESSION['insee']))
				{
					$this->render("message", array("title"=>"Indisponible", "message"=>"Le développement de cette page est encore en cours"));
				}
				else
				{
					$this->action_recherche();

				}
			}
			else{
				redirect('?controller=connexion&action=connexion');
			}
		}

		public function action_air() {
			if(test_login())
			{
				if(isset($_SESSION['insee']))
				{
					$this->render("message", array("title"=>"Indisponible", "message"=>"Le développement de cette page est encore en cours"));
				}
				else
				{
					$this->action_recherche();

				}
			}
			else{
				redirect('?controller=connexion&action=connexion');
			}
		}

		public function action_biodiv() {
			if(test_login())
			{
				if(isset($_SESSION['insee']))
				{
					$m = Model::get_model();
					$insee = $_SESSION['insee'];
					$infosVille = $m->getInfosVille($insee);
					$donneesINPN = $m->getDonneesINPN($insee);
					$this->render("biodiv", array(
													"donneesINPN" => $donneesINPN,
													"commune" => $infosVille['nom']
												));
				}
				else
				{
					$this->action_recherche();

				}
			}
			else{
				redirect('?controller=connexion&action=connexion');
			}
		}

		public function action_risques() {
			if(test_login())
			{
				if(isset($_SESSION['insee']))
				{
					$this->render("message", array("title"=>"Indisponible", "message"=>"Le développement de cette page est encore en cours"));
				}
				else
				{
					$this->action_recherche();

				}
			}
			else{
				redirect('?controller=connexion&action=connexion');
			}
		}

		public function action_urbanisme() {
			if(test_login())
			{
				if(isset($_SESSION['insee']))
				{
					$this->render("message", array("title"=>"Indisponible", "message"=>"Le développement de cette page est encore en cours"));
				}
				else
				{
					$this->action_recherche();

				}
			}
			else{
				redirect('?controller=connexion&action=connexion');
			}
		}



		/**
		 * Action par défaut du contrôleur (à définir dans les classes filles)
		 */
		public function action_default()
		{
			//var_dump("salut");
			$this->action_recherche();
		}
	}
