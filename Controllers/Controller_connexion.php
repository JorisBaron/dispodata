<?php
	class Controller_connexion extends Controller
	{
		public function action_connexion() {
			
			if(test_login()) {
				redirect("?controller=data&action=recherche");
			} else {
				$this->render("connexion");
			}
		}
		
		public function action_traitement_connexion() {
			
			if(test_login()) {
				redirect("?controller=data&action=recherche");
			}
			else
			{
				if(isset($_POST['login']) && trim($_POST['login'])!="" &&
				   isset($_POST['password']) && trim($_POST['password']!=""))
				{
					$m=Model::get_model();
					
					if($m->estInscrit($_POST['login'])) {
						if($m->isMailVerified($_POST['login']))
						{
							$logininfos=$m->getLoginInfos($_POST['login']);
							if(password_verify($_POST['password'], $logininfos['password']))
							{
								$_SESSION['login']=$_POST['login'];
								if(isset($_POST['cookie'])) {
									setcookie('login',$_POST['login'],time()+3600*24*365,null,null,false,true);
								}
								redirect("?controller=data&action=recherche");
							}
							else {
								$this->render('connexion',array('erreur'=>'L\'identifiant et le mot de passe ne correspondent pas')); //message erreur inexistant
							}
						}
						else {
							$this->render('attente_verif',array("mail"=>$_POST['login'])); //TODO la vue
						}
					}
					else {
						$this->render('connexion',array("erreur"=>"Aucun compte existant pour cette adresse")); //TODO message erreur inexistant
					}
					
				}
				else {
					$this->render('connexion');//TODO message d'erreur
				}
			}
		}
		
		public function action_deconnexion(){
			session_start_once();
			if(isset($_SESSION['login'])){
				unset($_SESSION['login']);
				if(isset($_COOKIE['login'])) {
					setcookie('login',"",time()-1,null,null,false,true);
					unset($_COOKIE['login']);
				}
			}
			$this->action_connexion();
		}
		
		public function action_default()
		{
			$this->action_connexion();
		}
	}