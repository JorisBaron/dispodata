<?php
	
	class Controller_inscription extends Controller
	{
		
		
		public function action_inscription()
		{
			if(test_login())
			{
				redirect("?controller=data&action=recherche");
			}
			else
			{
				$this->render("inscription");
			}
		}
		
		public function action_traitement_inscription()
		{
			
			if(test_login())
			{
				redirect("?controller=data&action=recherche");
			}
			else
			{
				if(isset($_POST['nom']) && trim($_POST['nom']) &&
				   isset($_POST['prenom']) && trim($_POST['prenom']) &&
				   isset($_POST['societe']) && trim($_POST['societe']) &&
				   isset($_POST['profession']) && trim($_POST['profession']) &&
				   isset($_POST['email']) && preg_match("#^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$#", $_POST['email']) &&
				   isset($_POST['password']) && trim($_POST['password']) &&
				   isset($_POST['password2']) && trim($_POST['password2']))
				{
					$form=array(
						"email"     =>trim($_POST['email']),
						"nom"       =>trim($_POST['nom']),
						"prenom"    =>trim($_POST['prenom']),
						"societe"   =>trim($_POST['societe']),
						"profession"=>trim($_POST['profession'])
					);
					
					$pass1=trim($_POST['password']);
					$pass2=trim($_POST['password2']);
					
					$m=Model::get_model();
					if(!$m->estInscrit($form['email']))
					{
						if(strlen($pass1)>=8)
						{
							if($pass1===$pass2)
							{
								
								
								$data=array_merge($form,
									array(
										"password"=>$pass1,
										"hash"    =>hash("sha256", $_POST['email'].time())
									)
								);
								
								$m->beginTransaction();
								if($m->insert_new_user($data))
								{
									require_once 'Utils/sendVerifMail.php';
									if(sendVerifMail($data['prenom'], $data['nom'], $data['email'], $data['hash']))
									{
										$m->commit();
										$this->render('attente_verif', array("mail"=>$data['email']));
									}
									else
									{
										$m->rollback();
										$this->render('inscription',
											array("erreur"=>"Impossible d'envoyer un mail de confirmation",
												  "form"  =>$form)
										);
									}
								}
								else
								{
									$m->rollback();
									$this->render('inscription',
										array("erreur"=>"Erreur lors de l'incription",
											  "form"  =>$form)
									);
								}
								
							}
							else
							{
								$this->render('inscription',
									array("erreur"=>"Mots de passe différents",
										  "form"  =>$form)
								);
							}
						}
						else
						{
							$this->render('inscription',
								array("erreur"=>"Mots de passe trop court",
									  "form"  =>$form)
							);
						}
					}
					else {
						$this->render('inscription',
							array("erreur"=>"L'adresse e-mail est déjà utilisée",
								  "form"  =>$form)
						);
					}
				}
				else
				{
					$this->render('inscription',
						array("erreur"=>"Informations manquantes ou vides",
													  "form"=>$_POST)
					);
				}
			}
			
		}
		
		public function action_verifmail() {
			if(isset($_GET['login']) && preg_match("#^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$#", $_GET['login']) &&
				isset($_GET['code'])) {
				
				$m=Model::get_model();

				if($m->estInscrit($_GET['login']) ) {
					if( !$m->isMailVerified($_GET['login'])) {
						if($m->verifMail($_GET['login'],$_GET['code'])) {
							$this->render('confirm');
						}
						else {
							$this->render('message',array(
								'title'=>'Erreur de confirmation',
								'message'=>'Une erreur a eu lieu lors de la confirmation'
							));
						}
					} else {
						$this->render('message',array(
							'title'=>'Confirmation',
							'message'=>'Le mail a déjà été verifié'
						));
					}
				} else {
					$this->render('message',array(
						'title'=>'Erreur',
						'message'=>'Ce compte n\'existe pas'
					));
				}
				
			}
		}

		public function action_renvoi_mail_confirmation() {
			if(isset($_GET['mail'])) {
				
				$m=Model::get_model();
				$data=$m->getInfoMail($_GET['mail']);
				require_once 'Utils/sendVerifMail.php';
				sendVerifMail($data['prenom'], $data['nom'], $data['login'], $data['code']);
				$this->render('attente_verif',array("mail"=>$data['login']));
			}
		}
		
		/**
		 * Action par défaut du contrôleur (à définir dans les classes filles)
		 */
		public function action_default()
		{
			$this->action_inscription();
		}
	}
	