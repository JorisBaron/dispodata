<?php require_once 'view_debut.php' ?>
<div class="container">
	<div class="boite" id="connexion">
		<h2>Adresse mail vérifiée</h2>
		<div class="content">
			<p>Votre adresse mail a été vérifiée, vous pouvez désormais utiliser vos identifiants sur notre site.</p>
			<a href="?controller=connexion&action=connexion" class="petitBouton">Se connecter</a>
		</div>
	</div>
</div>
<?php require_once 'view_fin.php' ?>