<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Bureau7 - Connexion</title>
		<link rel="stylesheet" href="Content/css/template.css">
		<link rel="stylesheet" href="Content/css/<?= $page ?>.css">
		<link rel="stylesheet" href="Content/css/table.css">

		<link href="Content/Select2/css/select2.css" rel="stylesheet" />
		<link rel="icon" type="image/jpg" href="Content/img/B7_noir_carre.jpg">


		<script src="Content/js/jquery-3.3.1.min.js"></script>
		<script src="Content/Select2/js/select2.js"></script>
		<script src="Content/Select2/js/i18n/fr.js"></script>

	</head>

	<body>
		<header>
			<a href="?controller=connexion&action=connexion" id="logo_lien">
				<img id="logo" src="Content/img/logo_noir.jpg" alt="logo"/>
			</a>
			<a href="?controller=connexion&action=<?= isset($_SESSION['login'])?'deconnexion':'connexion'?>" id="header_bouton">
				<span>
					<?= isset($_SESSION['login'])?'Déconnexion':'Connexion'?>
				</span>
			</a>
		</header>
		<nav id="menu">
			<ul>
				<?php
					if(isset($_SESSION['login'])) :
						require_once 'Utils/get_menu.php';
						$list=get_menu();
						?>
						<li>
							<a <?= $page==$list[0][0]?'class="ongletActif"':'' ?> href="?controller=data&action=<?= $list[0][0] ?>"> <!-- HREF A COMPLETER -->
								<img src="Content/img/<?= $list[0][2] ?>" alt="<?= $list[0][1] ?>"/> <span><?= $list[0][1] ?></span>
							</a>
						</li>
						<?php
						if(isset($_SESSION['insee'])) :
							unset($list[0]);
							foreach($list as $item) :
							?>
							<li>
								<a <?= $page==$item[0]?'class="ongletActif"':'' ?> href="?controller=data&action=<?= $item[0] ?>"> <!-- HREF A COMPLETER -->
									<img src="Content/img/<?= $item[2] ?>" alt="<?= $item[1] ?>"/> <span><?= $item[1] ?></span>
								</a>
							</li>
							<?php
							endforeach;
						endif;
					endif;
				?>
			</ul>
		</nav>

		<div id="cadre">
			<main>
