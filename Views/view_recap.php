<?php require_once 'view_debut.php' ?>

	<div class="container">

		<div class="boite">
			<h2>Récapitulatif</h2>
			<div class="content">
				<p>Vous trouverez ici le récapitulatif des informations concernant <span class="nomVille"><?= $commune?></span>.
				Vous pourrez trouver des informations plus détaillées dans les différents onglets.</p>
			</div>
		</div>

		<div class="boite">
			<h2><a href="?controller=data&action=meteo">Climat / Météo</a></h2>
			<div class="content">
				<p>Station la plus proche : <span class="nomVille"><?= $stationMeteo ?></span></p>
				<table>
					<tr>
						<th></th>
						<th>Jan.</th>
						<th>Fév.</th>
						<th>Mars</th>
						<th>Avril</th>
						<th>Mai</th>
						<th>Juin</th>
						<th>Juil.</th>
						<th>Août</th>
						<th>Sept.</th>
						<th>Oct.</th>
						<th>Nov.</th>
						<th>Déc.</th>
						<th>Année</th>
					</tr>
					<?php foreach($donneesMeteo as $cle=>$donnee): ?>
					<tr>
						<th><?= $cle ?></th>
						<?php if($donnee!==null) :
							foreach($donnee as $val): ?>
								<td><?= $val ?></td>
							<?php endforeach; ?>
							</tr>
						<?php else: ?>
							<td colspan="13">Données non disponibles</td>
						<?php endif; ?>
					<?php endforeach; ?>
					<caption>- : donnée égale à 0</caption>
				</table>
			</div>
			<!--<a class="bouton boiteBottom"></a>-->
		</div>

		<div class="boite">
			<h2><a href="?controller=data&action=biodiv">Biodiversite</a></h2>
			<div class="content">
				<p>Nombre d'espaces protégés, de Natura 2000 et de ZNIEFF continentales à <span class="nomVille"><?= $commune ?></span> : </p>
					<ul>
						<?php foreach($donneesINPN as $donnee) : ?>
						<li><?= $donnee['programme'] ?> : <?= $donnee['nb'] ?></li>
						<?php endforeach; ?>
					</ul>
			</div>
		</div>

		<div class="boite">
			<h2><a href="?controller=data&action=risques">Risques naturels et technologiques</a></h2>
			<div class="content">
				<p>Classe du potentiel Radon de <span class="nomVille"><?= $commune ?></span> : <?= $donneeRadon ?></p>
				<p>Nombre de risques GASPAR :</p>
				<ul>
					<?php foreach($donneesGASPAR as $cle=>$nb) : ?>
						<li><?= $cle ?> : <?=$nb ?></li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>


	</div>

<?php require_once 'view_fin.php' ?>
