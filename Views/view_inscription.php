<?php require_once 'view_debut.php' ?>
<div class="boite" id="inscription">
	<h2>Inscription</h2>
	<div class="content">
		<p>L'accès aux résultats de votre recherche est gratuit. Nous vous demandons uniquement de vous enregistrer via le
		questionnaire suivant. Un mail de validation de votre courriel vous sera envoyé.</p>
		<?php if(isset($erreur)) : ?>
			<p class="erreur"><?=$erreur?></p>
		<?php endif; ?>
		<form id="form_inscription" method="post" action="?controller=inscription&action=traitement_inscription">
			<div id="left">
				<p>Nom :</p>
				<input type="text" name="nom" value="<?= isset($form['nom'])?$form['nom']:'' ?>" required />
				<p>Prénom :</p>
				<input type="text" name="prenom" value="<?= isset($form['prenom'])?$form['prenom']:'' ?>" required />
				<p>Société / Collectivité :</p>
				<input type="text" name="societe" value="<?= isset($form['societe'])?$form['societe']:'' ?>" required />
				<p>Profession / Titre :</p>
				<input type="text" name="profession" value="<?= isset($form['profession'])?$form['profession']:'' ?>" required />
			</div>
			<div id="separator"></div>
			<div id="right">
				<p>Adresse e-mail :</p>
				<input type="text" name="email" value="<?= isset($form['email'])?$form['email']:'' ?>" pattern="^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$" required />
				<p>Mot de passe (8 caractères minimum) :</p>
				<input type="password" name="password" pattern="^\S{8,}$" required />
				<p>Confirmez votre mot de passe :</p>
				<input type="password" name="password2" required />
			</div>
		</form>
	</div>
	<input class="boiteBottom" type="submit" value="S'inscrire" form="form_inscription"/>
</div>
<?php require_once 'view_fin.php' ?>