			</main>
			
			<footer>
				<nav>
					<?php
						require_once 'Utils/get_bases_footer.php';
						$list=get_bases_footer();
						foreach($list as $item) :
					?>
					<a href="<?= $item[0] ?>" onclick="window.open(this.href); return false;" onkeypress="window.open(this.href); return false;"><img alt="<?= $item[1] ?>" src="Content/img/footer_logos/<?= $item[2] ?>"></a>
					<?php endforeach; ?>
				</nav>
				<div class="footer_infos">
					<p>16 rue Durand,<br/>34000 Montpellier, France</p>
					<p>Territoire - Habitat - Environnement</p>
					<p><a href="mailto:contact@bureau7%2efr">contact@bureau7.fr</a></p>					
					<p><a href="https://www.linkedin.com/company/bureau-7">LinkedIn</a></p>
				</div>	
			</footer>
		</div>
	</body>
</html>