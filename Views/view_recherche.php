<?php require_once 'view_debut.php' ?>
	<div class="container">
		<div class="boite" id="recherche">
			<h2>Rechercher</h2>
			<div class="content">
				<form id="form_recherche" method="post" action="?controller=data&action=traitement_recherche">
					<p><label for="inputVille">Entrez le nom ou le code postal de la commune :</label></p>
					<select class="js-select2" id="inputVille" name="insee">
						<option disabled selected></option>
					</select>
					<!--
					<div id='or'>
						<span>OU</span><hr/>
					</div>
					<p>Entrez le code postal de la commune :</p>
					<input oninput="onInput('inputCP')" id="inputCP" type="text" name="CP" list="villesCP"/>
					<datalist id="villesCP">
					
					</datalist>
					-->
					<script>
						var villes = <?= json_encode($villes)?>;
						$(document).ready(function() {
							$('.js-select2').select2({
								data: villes,
								placeholder: "Choisissez une commune",
								minimumInputLength: 1,
								language: "fr"
							});
						});
					</script>
				</form>
			</div>
			<input class="boiteBottom" type="submit" name="recherche" value="Accéder aux données" form="form_recherche"/>
		</div>
		<div class="rightPan">
			<p>Les bases avec lesquelles nous travaillons :</p>
			<div class="logos">	
				<?php	require_once 'Utils/get_bases_footer.php';
					$list=get_bases_footer();
					foreach($list as $item) : ?>
						<a href="<?= $item[0] ?>" onclick="window.open(this.href); return false;" onkeypress="window.open(this.href); return false;"><img alt="<?= $item[1] ?>" src="Content/img/logos/<?= $item[2] ?>"></a>
					<?php endforeach; ?>
			</div>		
		</div>
	</div>

<?php require_once 'view_fin.php' ?>