<?php require_once 'view_debut.php' ?>
<div class="container">
	<div class="boite" id="connexion">
		<h2>Attente de verification</h2>
		<div class="content">
			<p>L'adresse <?= hsc($mail) ?> est en attente de vérification.</p>
			<p>Pensez à vérifier vos spams !</p>
			<a href="?controller=inscription&action=renvoi_mail_confirmation&mail=<?= $mail ?>" class="petitBouton">Renvoyer le mail de confirmation</a>
		</div>
	</div>
</div>
<?php require_once 'view_fin.php' ?>
