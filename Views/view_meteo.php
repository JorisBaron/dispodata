<?php require_once 'view_debut.php' ?>

<div class="container">
  <div class="boite">
      <h2>Climat / Météo</h2>
      <div class="content">
        <p>Station la plus proche : <span class="nomVille"><?= $stationMeteo ?></span></p>
        <table>
          <tr>
            <th>Jan.</th>
            <th>Fév.</th>
            <th>Mars</th>
            <th>Avril</th>
            <th>Mai</th>
            <th>Juin</th>
            <th>Juil.</th>
            <th>Août</th>
            <th>Sept.</th>
            <th>Oct.</th>
            <th>Nov.</th>
            <th>Déc.</th>
            <th>Année</th>
          </tr>
          <?php foreach($donneesMeteo as $cle=>$donnee): ?>
            <tr><td colspan="13" class="titreMeteo"><?= $cle ?></td></tr>
            <tr>
              <?php if($donnee!==null) :
                foreach($donnee as $val): ?>
                <?php if(strpos($val,":")!==false) :
                  $val = explode(":",$val); ?>
                  <td class="dataMeteo"><?= $val[0] ?><br><span class="date"><?= $val[1] ?></span></td>
                <?php else : ?>
                  <td class="dataMeteo"><?= $val ?></td>
                <?php endif ?>
                <?php endforeach; ?>
            </tr>
              <?php else: ?>
              <td colspan="13" class="dataMeteo">Données non disponibles</td>
            <?php endif; ?>
          <?php endforeach; ?>
          <caption>- : donnée égale à 0</caption>
        </table>
      </div>
  </div>

<?php require_once 'view_fin.php' ?>
