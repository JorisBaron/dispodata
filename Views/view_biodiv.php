<?php require_once 'view_debut.php' ?>

<div class="container">
  <div class="boite">
      <h2>Biodiversité</h2>
      <div class="content">
        <p>Liste des espaces protégés, des Natura 2000 et des ZNIEFF continentales de
        <span class="nomVille"><?= $commune ?></span> : </p>
        <table>
          <th>Programme</th>
          <th>Nom du site</th>
          <?php foreach($donneesINPN as $cle=>$donnee) : ?>
            <tr>
              <th><?= $donnee['programme']?></th>
              <th class="siteINPN"><?= $donnee['nom']?></th>
            </tr>
          <?php endforeach ?>
        </table>
      </div>
  </div>
</div>
<?php require_once 'view_fin.php' ?>
