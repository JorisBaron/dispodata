<?php require_once 'view_debut.php' ?>
<div class="container">
	<div class="boite" id="connexion">
		<h2>Se connecter</h2>
		<div class="content">
			<?php if(isset($erreur)) : ?>
				<p class="erreur"><?=$erreur?></p>
			<?php endif;?>
			<form id="form_connexion" method="post" action="?controller=connexion&action=traitement_connexion">
				<p>Identifiant (e-mail) :</p>
				<input type="text" name="login" pattern="^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$" required />
				<p>Mot de passe :</p>
				<input type="password" name="password" required />
				<p><label for="cookie">Rester connecter</label> <input id="cookie" type="checkbox" name="cookie" /></p>
			</form>
		</div>
		<input class="boiteBottom" type="submit" value="Se connecter" form="form_connexion"/>
	</div>
	<div id="separator"></div>
	<div class="boite" id="inscription">
		<h2>Inscription</h2>
		<div class="content">
			<p>Enregistrez-vous dès maintenant pour avoir accès à notre service !</p>
			<p>L'accès aux résultats de votre recherche est gratuit. Nous vous demandons uniquement de vous enregistrer via le questionnaire suivant. Un mail de validation de votre courriel vous sera envoyé.</p>
		</div>
		<a href="?controller=inscription&action=inscription" class="bouton boiteBottom">S'enregistrer</a>
	</div>	
</div>
<?php require_once 'view_fin.php' ?>