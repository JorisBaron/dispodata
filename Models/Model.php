<?php

	class Model
	{
		private $bd;
		private static $instance=null;

		private function __construct()
		{
			global $loginBD,$passBD;
			try
			{
				$dsn="mysql:dbname=projetb7;host=localhost";      // A renseigner
				$login=$loginBD;    // A renseigner
				$password=$passBD; // A renseigner
				$this->bd=new PDO($dsn, $login, $password);
				$this->bd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->bd->query("SET nameS 'utf8'");
			}
			catch(PDOException $e)
			{
				die ('Echec connexion, erreur n°'.$e->getCode().':'.$e->getMessage());
			}
		}

		/**
		 * Méthode permettant de récupérer un modèle car le constructeur est privé (Implémentation du Design Pattern Singleton)
		 */
		public static function get_model()
		{
			if(is_null(self::$instance))
			{
				self::$instance=new Model();
			}
			return self::$instance;
		}

		/**
		 * insert un nouvel utilisateur
		 * @param $data array contenant les information de l'utilisateur (infos et hash de la confirmation)
		 */
		public function insert_new_user($data) {
			//TODO

			try
			{
				$req = $this->bd->prepare("INSERT INTO users(login, nom, prenom, societe, profession, password)
													VALUES (:login, :nom, :prenom, :societe, :profession, :pass);");
				$req->bindValue(":login", $data['email']);
				$req->bindValue(":nom", $data['nom']);
				$req->bindValue(":prenom", $data['prenom']);
				$req->bindValue(":societe", $data['societe']);
				$req->bindValue(":profession", $data['profession']);
				$req->bindValue(":pass", password_hash($data['password'], PASSWORD_DEFAULT));
				$req->execute();

				$req2 = $this->bd->prepare("INSERT INTO verifusers(login, code) VALUES (:login, :H);");
				$req2->bindValue(":login", $data['email']);
				$req2->bindValue(":H", $data['hash']);
				$req2->execute();

			} catch(PDOException $e) {
				return false;
			}
			return true;

		}

		public function beginTransaction() {
			$this->bd->beginTransaction();
		}

		public function commit() {
			$this->bd->commit();
		}

		public function rollback() {
			$this->bd->rollBack();
		}

		public function estInscrit($mail) {
			$req = $this->bd->prepare('SELECT * FROM users WHERE login=:mail');
			$req->bindValue(":mail",$mail);
			$req->execute();
			return ($req->fetch())!=false;
		}

		public function verifMail($mail, $hash) {
			$reqGet = $this->bd->prepare("SELECT * FROM verifusers WHERE login=:login;");
			$reqGet->bindValue(":login",$mail);
			$reqGet->execute();
			$res = $reqGet->fetch(PDO::FETCH_ASSOC);

			if($res!=false && $res['code']==$hash) {

				$reqConfirm = $this->bd->prepare('DELETE FROM verifusers WHERE login=:login');
				$reqConfirm->bindValue(':login',$res['login']);
				return $reqConfirm->execute();
			}
			else {
				return false;
			}
		}

		public function isMailVerified($mail) {
			$req = $this->bd->prepare('SELECT * FROM verifusers WHERE login=:mail');
			$req->bindValue(":mail",$mail);
			$req->execute();
			return ($req->fetch())==false;
		}

		public function getLoginInfos($login){
			$req = $this->bd->prepare('SELECT login,password FROM users WHERE login=:mail');
			$req->bindValue(":mail",$login);
			$req->execute();
			return $req->fetch(PDO::FETCH_ASSOC);
		}

		public function getInfoMail($mail) {
			$req = $this->bd->prepare('SELECT login, nom, prenom, code FROM verifusers NATURAL JOIN users WHERE login=:mail');
			$req->bindValue(":mail",$mail);
			$req->execute();
			return $req->fetch(PDO::FETCH_ASSOC);
		}

		public function getVillesSelectRecherche() {
			$req = $this->bd->prepare("SELECT insee as id, CONCAT(nom, ' (',CP,')') as text FROM villes ORDER BY nom;");
			$req->execute();
			return $req->fetchAll(PDO::FETCH_ASSOC);
		}

		public function getInfosVille($insee) {
			$req = $this->bd->prepare('SELECT nom, insee, CP, departement, code_departement, region, code_region,
      									 ST_X(coordonnees) as longitude, ST_Y(coordonnees) as latitude FROM villes WHERE insee=:insee;');
			$req->bindValue(":insee",$insee);
			$req->execute();
			return $req->fetch(PDO::FETCH_ASSOC);
		}

		public function getStationsMeteo(){
			$req = $this->bd->prepare('SELECT indicatif, ST_X(coordonnees) as longitude, ST_Y(coordonnees) as latitude FROM meteofrance;');
			$req->execute();
			return $req->fetchAll(PDO::FETCH_ASSOC);
		}

		public function getDonneesMeteo($indicatif) {
			$req = $this->bd->prepare('SELECT commune, temperatureMax, temperatureMin, nbJoursTempMaxSup25,
											nbJoursTempMaxInf0, recordHauteurPrecipitations, hauteurPrecipitationsMoyenne,
											degresJoursUnifies, rayonnementGlobal, dureeInsolation, recordRafaleVent, nbJoursNeige
												from meteofrance WHERE indicatif=:indi;');
			$req->bindValue(":indi",$indicatif);
			$req->execute();
			return $req->fetch(PDO::FETCH_ASSOC);
		}

		public function getDonneesMeteoRecap($indicatif) {
			$req = $this->bd->prepare('SELECT commune, nbJoursTempMaxSup25, hauteurPrecipitationsMoyenne, degresJoursUnifies, dureeInsolation
										from meteofrance WHERE indicatif=:indi;');
			$req->bindValue(":indi",$indicatif);
			$req->execute();
			return $req->fetch(PDO::FETCH_ASSOC);
		}

		public function getDonneesINPN($insee) {
			$req = $this->bd->prepare('SELECT programme, nom FROM inpn WHERE insee=:insee');
			$req->bindValue(":insee",$insee);
			$req->execute();
			return $req->fetchAll(PDO::FETCH_ASSOC);
		}

		public function getDonneesINPNRecap($insee) {
			$req = $this->bd->prepare(
'SELECT * FROM (
    SELECT programme, Count(*) AS nb FROM inpn WHERE insee=:insee GROUP BY programme
) as tabNb
NATURAL RIGHT JOIN (
    SELECT DISTINCT programme FROM inpn ORDER BY programme
) as tabProg');
			$req->bindValue(":insee",$insee);
			$req->execute();
			return $req->fetchAll(PDO::FETCH_ASSOC);
		}


		public function getDonneesRadon($insee){
			$req=$this->bd->prepare('SELECT classe_potentiel FROM radon WHERE insee=:insee');
			$req->bindValue(":insee",$insee);
			$req->execute();
			return $req->fetch(PDO::FETCH_ASSOC);
		}
		
		public function getDonneesGASPARRecap($insee) {
			
			$jsonStr = file_get_contents('https://public.opendatasoft.com/api/records/1.0/search/?dataset=gaspar-risques&facet=lib_risque_long&refine.cod_commune='.$insee);
			$json = json_decode($jsonStr,true);
			return $json['facet_groups'][0]['facets'];
		}
}
