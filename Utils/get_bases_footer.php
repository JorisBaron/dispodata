<?php
	function get_bases_footer() {
		$file=@fopen('Content/liste_footer.txt','r');
		if($file) {
			$arr=array();
			while( ($ligne=fgets($file))!==false) {
				$ligne=trim($ligne);
				if($ligne!=="") {
					$arr[]=explode('|',$ligne);
				}
			}
			fclose($file);
			return $arr;
		}
		else {
			die("Erreur lors de l'ouverture du fichier liste_footer.txt");
		}
	}