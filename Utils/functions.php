<?php
	function hsc($str) {
		return htmlspecialchars($str, ENT_QUOTES);
	}
	
	function test_login() {
		session_start_once();
		if(isset($_COOKIE['login'])){
			$_SESSION['login']=$_COOKIE['login'];
			setcookie('login',$_COOKIE['login'],time()+3600*24*365,null,null,false,true);
		}
		
		return isset($_SESSION['login']);
	}
	
	function session_start_once() {
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
	}
	
	function redirect($url) {
		header("Location: ".MON_URL.$url , true,303);
		die();
	}
	
	function calculOrthodromie($lonA,$latA,$lonB,$latB) {
		
		return 60*rad2deg(acos(sin(deg2rad($latA))*sin(deg2rad($latB))+cos(deg2rad($latA))*cos(deg2rad($latB))*cos(deg2rad($lonB-$lonA))));
	}