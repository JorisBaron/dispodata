<?php
	function get_menu() {
		$file=@fopen('Content/liste_nav.txt','r');
		if($file) {
			$arr=array();
			while( ($ligne=fgets($file))!==false) {
				$ligne=trim($ligne);
				if($ligne!=="") {
					$arr[]=explode('|',$ligne);
				}
			}
			fclose($file);
			return $arr;
		}
		else {
			die("Erreur lors de l'ouverture du fichier liste_nav.txt");
		}
	}
