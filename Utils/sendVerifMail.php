<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	
	require_once 'Utils/PHPMailer/src/Exception.php';
	require_once 'Utils/PHPMailer/src/PHPMailer.php';
	require_once 'Utils/PHPMailer/src/SMTP.php';
	
	function sendVerifMail($prenom,$nom,$mail,$hash) {
		global $serveurMail,$loginMail,$passMail,$senderMail,$portMail;
		
		//suite
		$lien=MON_URL."/index.php?controller=inscription&action=verifmail&login=".$mail."&code=".$hash;
		$message=file_get_contents('Content/messageVerification.html');
		$message=str_replace('{lien}',$lien,$message);
		
		try {
			date_default_timezone_set('Etc/UTC');
			$mailer = new PHPMailer;
			$mailer->CharSet = 'UTF-8';
			$mailer->Encoding = 'base64';
			
			$mailer->isSMTP();
			$mailer->SMTPDebug=0;
			$mailer->Host=$serveurMail;
			$mailer->Port=$portMail;
			$mailer->SMTPAuth=true;
			$mailer->Username=$loginMail;
			$mailer->Password=$passMail;
			
			$mailer->setLanguage('fr', '/Utils/PHPMailer/language/');
			$mailer->setFrom($senderMail,"Bureau 7");
			$mailer->addAddress($mail,$prenom.' '.$nom);
			$mailer->Subject='Vérification d\'adresse mail';
			$mailer->isHTML(true);
			$mailer->msgHTML($message);
			
			return $mailer->send();
			
		} catch(Exception $e)
		{
			return false;
			//die('Erreur lors de l\'envoi <br> ligne '.$e->getLine().'<br>'.$e->getMessage());
		}
		
	}